//
//  CommentTableViewCell.swift
//  iOS-101
//
//  Created by 1 on 27.06.18.
//  Copyright © 2018 Maksim Zybin. All rights reserved.
//

import UIKit

class CommentTableViewCell: UITableViewCell {

    static let nibName = "CommentTableViewCell"
    
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    func customize (comment: CommentModel){
        nameLabel.text = comment.name
        commentLabel.text = comment.body
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
